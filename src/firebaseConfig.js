import firebase from 'firebase'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/functions'

// firebase init goes here
const config = {
  apiKey: 'AIzaSyCa_MxWXD6gJja2vMg5zgHBWniX8Xn_3fY',
  authDomain: 'job-research-manager.firebaseapp.com',
  databaseURL: 'https://job-research-manager.firebaseio.com',
  projectId: 'job-research-manager',
  storageBucket: 'job-research-manager.appspot.com',
  messagingSenderId: '106627414462'
}
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser
const fireb = firebase
const functions = firebase.functions()

// date issue fix according to firebase
const settings = {
  timestampsInSnapshots: true
}
db.settings(settings)

// firebase collections


export {
  db,
  auth,
  currentUser,
  fireb,
  functions
}
