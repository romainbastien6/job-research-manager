const fb = require('../../firebaseConfig')

export default {

  state: {
    user: null
  },

  getters: {
    getUser(state, getters){
      return state.user
    }
  },

  mutations: {
    setUser (state, payload) {
      state.user = payload
    }
  },

  actions: {

    signUserUp({commit}, payload){
      console.log('requesting Firebase')
      fb.auth.createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            const newUser = {
              id: user.uid,
              name: user.displayName,
              email: user.email,
              photoUrl: user.photoURL
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            console.log(error)
          }
        )
    },

    signUserIn({commit}, payload){
      fb.auth.signInWithEmailAndPassword(payload.email, payload.password)
        // .then(
        //   user => {
        //     const newUser = {
        //       id: user.uid,
        //       name: user.displayName,
        //       email: user.email,
        //       photoUrl: user.photoURL
        //     }
        //     console.log('signUserIn')
        //     console.log(payload)
        //     commit('setUser', newUser)
        //   }
        // )
        // .catch(
        //   error => {
        //     console.log(error)
        //   }
        // )
    },

    signUserInGoogle ({commit}) {
      fb.auth.signInWithPopup(new fb.fireb.auth.GoogleAuthProvider())
        .then(
          user => {
            const newUser = {
              id: user.uid,
              name: user.displayName,
              email: user.email,
              photoUrl: user.photoURL
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            console.log(error)
          }
        )
    },

    autoSignIn ({commit}, payload) {
      console.log('autoSignIn')
      console.log(Date.now())
      console.log(payload)
      commit('setUser', {
        id: payload.uid,
        name: payload.displayName,
        email: payload.email,
        photoUrl: payload.photoURL
      })
    },

    logOut ({commit}) {
      fb.auth.signOut()
      commit('setUser', null)
    }

  }
}
