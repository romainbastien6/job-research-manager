const fb = require('../../firebaseConfig')

export default {

  state: {
    userInfo: null
  },

  getters: {
    getUserInfo (state) {
      return state.userInfo
    }
  },

  mutations: {
    setUserInfo(state, payload){
      state.userInfo = payload
    }
  },

  actions: {
    fetchUserInfo ({commit, dispatch}, payload) {
      fb.db.collection(`users`).doc(payload.id).get()
        .then(doc => {
          if (doc.exists) {
            console.log(doc.data())
            commit('setUserInfo', doc.data())
          } else {
            dispatch('createUserProfile', payload)
          }
        })
        .catch(error => {
          console.log(error)
        })
    },

    createUserProfile ({commit}, payload) {
      fb.db.collection('users').doc(payload.id).set({
        id: payload.id
      })
        .then(
          commit('setUserInfo', payload)
        )
        .catch(error => {
          console.log(error)
        })
    }
  }
}
