const fb = require('../../firebaseConfig')

export default {

  state: {
    campaigns: [],
    activeCampaign: null
  },

  getters: {
    getCampaigns (state, getters) {
      return state.campaigns
    },

    getActiveCampaign (state, getters) {
      return state.activeCampaign
    }
  },

  mutations: {
    setCampaign (state, payload) {
      state.campaigns = [...state.campaigns, payload]
    },

    setActiveCampaign (state, payload) {
      state.activeCampaign = payload
    },

    resetCampaigns (state) {
      state.campaigns = []
    },

    setDefaultCV (state, payload) {
      state.activeCampaign.defaultCV = payload
    },

    setDefaultMailTemplate (state, payload) {
      state.activeCampaign.defaultMailTemplate = payload
    },

    setDefaultMotivationLetterTemplate (state, payload) {
      state.activeCampaign.defaultMotivationLetterTemplate = payload
    }
  },

  actions: {

    /* --- Read --- */

    async fetchCampaigns (context) {
      if (context.state.campaigns !== []) {
        context.commit('resetCampaigns')
      }
      console.log(`users/${context.rootState.user.user.id}/campaigns`)
      const campaignsRaw = await fb.db.collection(`users/${context.rootState.user.user.id}/campaigns`).get()
      campaignsRaw.forEach(campaign => context.commit('setCampaign', campaign.data()))
      console.log(`users/${context.state.user.id}/campaigns`)
      console.log(campaignsRaw)
      console.log('Flag 2')
    },

    async fetchCampaign ({commit, rootState}, payload) {
      const campaignRaw = await fb.db.collection(`users/${rootState.user.user.id}/campaigns`).doc(payload).get();
      const campaign = campaignRaw.data();
      commit('setActiveCampaign', campaign)
    },

    /* --- Create --- */

    async createNewCampaign ({commit, rootState}, newCampaign) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns`).doc(`${newCampaign.info.id}`).set({
          title: newCampaign.title,
          contractType: newCampaign.contractType,
          expectedSalary: newCampaign.expectedSalary,
          geoField: newCampaign.geoField,
          sector: newCampaign.sector,
          info: newCampaign.info,
          active: newCampaign.active
        });
        console.log('Campaign created');
        commit('setCampaign', newCampaign);
      } catch (error) {
        console.log(error)
      }
    },

    async uploadCV ({commit, rootState}, payload) {
      try {
        const cvPath = `${rootState.user.user.id}/${rootState.campaigns.activeCampaign.info.id}/defaultCV.pdf`
        const ref = fb.fireb.storage().ref();
        await ref.child(`${rootState.user.user.id}/${rootState.campaigns.activeCampaign.info.id}/defaultCV.pdf`).put(payload)
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns`).doc(`${rootState.campaigns.activeCampaign.info.id}`).update({
          defaultCV: cvPath
        })
        console.log('File uploaded')
        commit('setDefaultCV', cvPath)
      }

      catch (e) {
        console.log(e)
      }
    },

    async uploadDefaultMailTemplate ({rootState, commit}, payload) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns`).doc(`${rootState.campaigns.activeCampaign.info.id}`).update({
          defaultMailTemplate: payload
        })
        commit('setDefaultMailTemplate', payload)
      }

      catch (e) {
        console.log(e)
      }
    },

    async uploadDefaultMotivationLetterTemplate ({rootState, commit}, payload) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns`).doc(`${rootState.campaigns.activeCampaign.info.id}`).update({
          defaultMotivationLetterTemplate: payload
        })
        commit('setDefaultMotivationLetterTemplate', payload)
      }
      
      catch (e) {
        console.log(e)
      }
    }
  }
}
