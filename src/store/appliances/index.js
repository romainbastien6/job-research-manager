const fb = require('../../firebaseConfig')
const axios = require('axios')

export default {

  state: {
    appliances: [],
    activeAppliance: null
  },

  getters: {
    getAppliances(state, getters) {
      return state.appliances
    },

    getActiveAppliance(state, getters) {
      return state.activeAppliance
    }
  },

  mutations: {
    setAppliance(state, payload) {
      state.appliances = [...state.appliances, payload]
    },

    setActiveAppliance(state, payload) {
      state.activeAppliance = payload
    },

    resetAppliances(state) {
      state.appliances = []
    },

    setCompanyInfos(state, payload) {
      state.activeAppliance.company = payload
    },

    setPositionInfos(state, payload) {
      state.activeAppliance.position = payload
    },

    setContactInfos(state, payload) {
      state.activeAppliance.contact = payload
    },

    setMotivationContent(state, payload) {
      state.activeAppliance.motivationContent = payload
    }

  },

  actions: {

    /* --- Read --- */

    async fetchAppliances(context) {
      if (context.state.appliances !== []) {
        context.commit('resetAppliances')
      }

      console.log(`users/${context.rootState.user.user.id}/campaigns/${context.rootState.campaigns.activeCampaign.info.id}/appliances`)

      const appliancesRaw = await fb.db.collection(`users/${context.rootState.user.user.id}/campaigns/${context.rootState.campaigns.activeCampaign.info.id}/appliances`).get();
      appliancesRaw.forEach(appliance => context.commit('setAppliance', appliance.data()))
    },

    async fetchAppliance({commit, rootState}, payload) {
      const applianceRaw = await fb.db.collection(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`).doc(payload).get();
      const appliance = applianceRaw.data();
      commit('setActiveAppliance', appliance)
    },

    /* --- Create --- */

    async createNewAppliance({commit, rootState}, newAppliance) {
      try {
        console.log(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`)
        console.log('flag')
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`).doc(newAppliance.info.id).set({
          companyName: newAppliance.companyName,
          positionTitle: newAppliance.positionTitle,
          path: {
            currentStep: newAppliance.path.currentStep
          },
          company: {
            name: newAppliance.company.name,
            adress1: newAppliance.company.adress1,
            adress2: newAppliance.company.adress2,
            city: newAppliance.company.city,
            zipcode: newAppliance.company.zipcode,
            country: newAppliance.company.country,
            activities: newAppliance.company.activities,
            geoFields: newAppliance.company.geoFields,
            revenue: newAppliance.company.revenue,
            workforce: newAppliance.company.workforce,
            otherInformations: newAppliance.company.otherInformations
          },
          contact: {
            name: newAppliance.contact.name,
            position: newAppliance.contact.position,
            mail: newAppliance.contact.mail,
            phone: newAppliance.contact.phone,
            linkedInProfile: newAppliance.contact.linkedInProfile,
            twitterProfile: newAppliance.contact.twitterProfile
          },
          position: {
            title: newAppliance.position.title,
            missions: newAppliance.position.missions,
            skills: newAppliance.position.skills,
            qualities: newAppliance.position.qualities
          },
          info: {
            id: newAppliance.info.id,
            campaign: newAppliance.info.campaign,
            user: newAppliance.info.user
          }
        });
        console.log('Appliance created')
      } catch (e) {
        console.log(e);
      }
    },

    /* --- Update --- */

    async updateCompanyInfos({commit, rootState}, payload) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`).doc(rootState.appliances.activeAppliance.info.id).update({
          company: payload
        })
        console.log('Company Infos Updated')
        commit('setCompanyInfos', payload)
      } catch (e) {
        console.log(e)
      }
    },

    async updatePositionInfos({commit, rootState}, payload) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`).doc(rootState.appliances.activeAppliance.info.id).update({
          position: payload
        })
        console.log('Position Infos Updated')
        commit('setPositionInfos', payload)
      } catch (e) {
        console.log(e)
      }
    },

    async updateContactInfos({commit, rootState}, payload) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`).doc(rootState.appliances.activeAppliance.info.id).update({
          contact: payload
        })
        console.log('Contact Infos Updated')
        commit('setContactInfos', payload)
      } catch (e) {
        console.log(e)
      }
    },

    async updateMotivationContent({commit, rootState}, payload) {
      try {
        await fb.db.collection(`users/${rootState.user.user.id}/campaigns/${rootState.campaigns.activeCampaign.info.id}/appliances`).doc(rootState.appliances.activeAppliance.info.id).update({
          motivationContent: payload
        })
        commit('setMotivationContent', payload)
      } catch (e) {
        console.log(e)
      }
    },

    async sendAppliance({commit, rootState}, payload) {
      axios.post('http://localhost:4000/', {
          toSend: payload
        }, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }
      )
        .then(() => {
          console.log('Appliance sent')
        })
        .catch((e) => {
          console.log(e)
        })


    }
  }

}
