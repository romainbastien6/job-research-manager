import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'


import user from './user'
import userInfo from './userInfo'
import campaigns from './campaigns'
import appliances from './appliances'

Vue.use(Vuex)


export const store = new Vuex.Store({
  modules: {
    user: user,
    userInfo: userInfo,
    campaigns: campaigns,
    appliances: appliances
  },
  plugins: [
    createPersistedState({
      reducer: state => ({
        user: state.user,
        userInfo: state.userInfo,
        activeCampaign: state.campaigns.activeCampaign,
        activeAppliance: state.appliances.activeAppliance
      })
    })
  ]
})
