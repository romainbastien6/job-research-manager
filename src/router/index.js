import Vue from 'vue'
import Router from 'vue-router'
import UserDashboard from '@/views/UserDashboard'
import Connection from '@/views/Connection'
import CampaignDetails from '@/views/CampaignDetails'
import ApplianceDetails from '@/views/ApplianceDetails'
import CreateCampaign from '@/views/CreateCampaign'
import CreateAppliance from '@/views/CreateAppliance'
import ViewCampaignCV from '@/views/ViewCampaignCV'
import CreateMailTemplate from '@/views/CreateMailTemplate'
import MotivationLetterDefaultTemplate from '@/views/MotivationLetterDefaultTemplate'
import ApplianceMotivationLetterContent from '@/views/ApplianceMotivationLetterContent'

import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'UserDashboard',
      component: UserDashboard,
      beforeEnter: AuthGuard
    },
    {
      path: '/connection',
      name: 'Connection',
      component: Connection
    },
    {
      path: '/campaigns/:id',
      name: 'CampaignDetails',
      component: CampaignDetails,
      beforeEnter: AuthGuard
    },
    {
      path: '/appliances/:id',
      name: 'ApplianceDetails',
      component: ApplianceDetails,
      beforeEnter: AuthGuard
    },
    {
      path: '/create-campaign',
      name: 'CreateCampaign',
      component: CreateCampaign,
      beforeEnter: AuthGuard
    },
    {
      path: '/create-appliance',
      name: 'CreateAppliance',
      component: CreateAppliance,
      beforeEnter: AuthGuard
    },
    {
      path: '/view-campaign-cv',
      name: 'ViewCampaignCV',
      component: ViewCampaignCV,
      beforeEnter: AuthGuard
    },
    {
      path: '/create-mail-template',
      name: 'CreateMailTemplate',
      component: CreateMailTemplate,
      beforeEnter: AuthGuard
    },
    {
      path: '/create-motivation-letter',
      name: 'MotivationLetterDefaultTemplate',
      component: MotivationLetterDefaultTemplate,
      beforeEnter: AuthGuard
    },
    {
      path: '/create-motivation-content',
      name: 'ApplianceMotivationLetterContent',
      component: ApplianceMotivationLetterContent,
      beforeEnter: AuthGuard
    }

  ],
  mode: 'history'
})


